<?php
require 'func.php';

//cek apakah tombol submit ditekan atau belum 
if( isset($_POST["submit"]) ) {
    

    

    //cek apakah data berhasil di tambahkan atau tidak
    if( tambah($_POST) > 0  ){
        echo "
                <script>
                    alert('data berhasil ditambahkan!')
                    document.location.href = 'index.php';
                </script>
        ";
    } else { 
        echo "
                <script>
                    alert('data gagal ditambahkan!')
                    document.location.href = 'index.php';
                </script>
        ";
    }

}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Tambah Data Dokter</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>
    <h1>Tambah Data Dokter</h1>

    <form action="" method="post">
        <ul>
            <li>
                <label for="nama">Nama Dokter   : <br><br> </label>
                <input type="text" name="nama" id="nama" required>
            </li>
            
            <li>
            <label for="data">Data Dokter     : <br><br></label>
                <input type="text" name="data" id="data" required>
            </li>
           
             <li>
            <label for="email">Email          : <br><br> </label>
                <input type="text" name="email" id="email" required>
            </li>
          
            <li>
            <label for="gambar">Gambar       : <br><br> </label>
                <input type="text" name="gambar" id="gambar" required>
            </li>
            <br><br>
            <li>
                <button type="submit" name="submit" class="btn btn-primary">Tambah Data!</button>
            </li>
         
        </ul>
</body>
</html>