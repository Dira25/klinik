<?php
    $conn = mysqli_connect("localhost", "root", "", "makn");

    $result = mysqli_query($conn, "SELECT * FROM xi_rpl2");

//    while ( $mhs = mysqli_fetch_row($result) ) {
//     //  var_dump($mhs["nama"]);
//    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Data Dokter </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>
    <h1>Data Dokter</h1>

    <a href="tambah.php" class="btn btn-success">Tambah Data Dokter</a>
    
    <br><br>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Nama Dokter</th>
            <th>Data Dokter</th>
            <th>Email</th>
            <th>Gambar</th>
            
        </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        <?php while($row = mysqli_fetch_assoc($result) ) : ?>
        <tr>
            <td><?= $i; ?></td>
            <td>
                <a href="ubah.php" class="btn btn-warning">Ubah</a>
                <a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin?');" class="btn btn-danger">Hapus</a>
            </td>
            <td><?= $row["nama"] ?></td>
            <td><?= $row["alamat"] ?></td>
            <td><?= $row["email"] ?></td>
            <td><img src="img/<?= $row["gambar"] ?>" width="60" height="90"></td>
        </tr>
        <?php $i++; ?>
        <?php endwhile; ?>
    </tbody>
    <tfoot>
    <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Nama Dokter</th>
            <th>Data Dokter</th>
            <th>Email</th>
            <th>Gambar</th>
            
        </tr>
    </tfoot>
    </table>


    <style>
        /* .btn-tambah {
    text-align: center;
    width: 100%;
    font-size: 16px;
    border-radius: 5px;
    padding: 14px 25px;
    border: none;
    font-weight: 500;
    background-color: green;
    color: white;
    cursor: pointer;
    margin-top: 35px;
    color: black;
  }
  .btn-tambah:hover {
    box-shadow: 0px 3px 8px rgba(0, 0, 0, 0.05);
  }

        .btn-ubah {
    text-align: center;
    width: 100%;
    font-size: 16px;
    border-radius: 5px;
    padding: 14px 25px;
    border: none;
    font-weight: 500;
    background-color: yellow;
    color: white;
    cursor: pointer;
    margin-top: 35px;
    color: black;
  }
  .btn-ubah:hover {
    box-shadow: 0px 3px 8px rgba(0, 0, 0, 0.05);
  }

  .btn-hapus {
    text-align: center;
    width: 100%;
    font-size: 16px;
    border-radius: 5px;
    padding: 14px 25px;
    border: none;
    font-weight: 500;
    background-color: red;
    color: white;
    cursor: pointer;
    margin-top: 35px;
    color: black;
  }
  .btn-hapus:hover {
    box-shadow: 0px 3px 8px rgba(0, 0, 0, 0.05);
  }

  table{
    width: 100%;

  } */

    </style>
</body>
</html>