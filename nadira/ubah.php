
<?php
require 'func.php';

//ambil data di URL
$id = $_GET["id"];
//query data siswa berdasarkan di
$siswa = query("SELECT * FROM xi_rpl2 WHERE id = '$id'")[0];

//cek apakah tombol submit ditekan atau belum 
if( isset($_POST["submit"]) ) {
    

    //cek apakah data berhasil diubah atau tidak
    if( ubah($_POST) > 0  ) {
        echo "
                <script>
                    alert('data berhasil diubah!')
                    document.location.href = 'index.php';
                </script>
        ";
    } else { 
        echo "
                <script>
                    alert('data gagal diubah!')
                    document.location.href = 'index.php';
                </script>
        ";
    }

}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Ubah Data Dokter</title>
    
</head>
<body>
    <h1>Ubah Data Dokter</h1>

    <form action="" method="post">
        <input type="hidden" name="id" value="<?= $siswa["id"]?>">
        <ul>
            <li>
                <label for="nama">Nama Dokter   :</label>
                <input type="text" name="nama" id="nama" required 
                value="<?= $siswa["nama"]; ?>">
            </li>
        
            <li>
            <label for="alamat">Data Dokter     :</label>
                <input type="text" name="alamat" value="<?=$siswa["alamat"]?>" >
            </li>
            
            <li>
            <label for="email">Email            :</label>
                <input type="text" name="email" value="<?=$siswa["email"]?>">
            </li>
            <li>
            <label for="gambar">Gambar          :</label>
                <input type="text" name="gambar" id="gambar">
            </li>
            <li>
                <button type="submit" name="submit">Ubah Data!</button>
            </li>
        </ul>
</body>
</html>